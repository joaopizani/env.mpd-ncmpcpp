#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" && pwd)"

MPDPASSWD="${1}"

MPDSCRCONFDIR_DEFAULT="${HOME}/.mpdscribble"
MPDSCRCONFDIR="$(readlink -f "${2:-"${MPDSCRCONFDIR_DEFAULT}"}")"

MPDSCRLOG="${MPDSCRCONFDIR}/mpdscribble.log"
MPDSCRLASTFMJOURNAL="${MPDSCRCONFDIR}/lastfm.journal"

read    -p 'Last.fm username > ' LASTFMUSER
read -s -p 'Last.fm password > ' LASTFMPASSWD  &&  echo ""
LASTFMPASSWD_MD5="$(echo -n "${LASTFMPASSWD}" | md5sum | cut -f 1 -d " ")"


mkdir -p "${MPDSCRCONFDIR}"  &&  touch "${MPDSCRLOG}" "${MPDSCRLASTFMJOURNAL}"
cat <<EOF > "${MPDSCRCONFDIR}/mpdscribble.conf"
[mpdscribble]
host = ${MPDPASSWD}@localhost
log = ${MPDSCRLOG}
verbose = 2

[last.fm]
url = http://post.audioscrobbler.com/
username = ${LASTFMUSER}
password = ${LASTFMPASSWD_MD5}
journal = ${MPDSCRLASTFMJOURNAL}
EOF
