#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" && pwd)"

MPDPASSWD="${1}"

XDG_MUSIC_HOME="$(xdg-user-dir MUSIC)"
CONFIGHOME="${XDG_CONFIG_HOME:-"${HOME}/.config"}"
MUSICHOME="${XDG_MUSIC_HOME:-"${HOME}/Music"}"
LOCALSHAREHOME="${XDG_DATA_HOME:-"${HOME}/.local/share"}"

MPDCONFDIR_DEFAULT="${CONFIGHOME}/mpd"
MPDLOCALDIR_DEFAULT="${LOCALSHAREHOME}/mpd"

MPDMUSICDIR="${2:-"${MUSICHOME}"}"
MPDCONFDIR="$(readlink -f "${4:-"${MPDCONFDIR_DEFAULT}"}")"
MPDLOCALDIR="$(readlink -f "${5:-"${MPDLOCALDIR_DEFAULT}"}")"

MPDPLAYLISTDIR_DEFAULT="${MPDLOCALDIR}/playlists"
MPDPLAYLISTDIR="${3:-"${MPDPLAYLISTDIR_DEFAULT}"}"


read -n 1 -p 'Type 1 for listening openly, 2 (or anything else) for localhost ONLY > ' MPDBINDADDR_CHOICE  &&  echo ""
MPDNETWORKIP="$(hostname --all-ip-addresses | cut -f 1 -d " ")"
                                          MPDBINDSTATEMENT="bind_to_address \"127.0.0.1\""
[[ "${MPDBINDADDR_CHOICE}" == "1" ]]  &&  MPDBINDSTATEMENT+="\nbind_to_address \"${MPDNETWORKIP}\""


mkdir -p "${MPDCONFDIR}" "${MPDPLAYLISTDIR}" "${MPDLOCALDIR}"  &&  touch "${MPDLOCALDIR}"/{database,sticker.sql}
cat <<EOF > "${MPDCONFDIR}/mpd.conf"
music_directory      "${MPDMUSICDIR/#$HOME/\~}"
playlist_directory   "${MPDPLAYLISTDIR/#$HOME/\~}"
db_file              "${MPDLOCALDIR/#$HOME/\~}/database"
sticker_file         "${MPDLOCALDIR/#$HOME/\~}/sticker.sql"
log_file             "syslog"

password "${MPDPASSWD}@read,add,control,admin"

restore_paused "yes"
replaygain "auto"

audio_output {
    type "pulse"
    name "MPDPulseAudio"
}

$(echo -e "${MPDBINDSTATEMENT}")
EOF
