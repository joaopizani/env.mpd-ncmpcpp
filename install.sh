#!/usr/bin/env bash
DIR="$(cd -P "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )" && pwd)"

XDG_MUSIC_HOME="$(xdg-user-dir MUSIC)"
CONFIGHOME="${XDG_CONFIG_HOME:-"${HOME}/.config"}"
MUSICHOME="${XDG_MUSIC_HOME:-"${HOME}/Music"}"
LOCALSHAREHOME="${XDG_DATA_HOME:-"${HOME}/.local/share"}"

MPDCONFDIR_DEFAULT="${CONFIGHOME}/mpd"
MPDLOCALDIR_DEFAULT="${LOCALSHAREHOME}/mpd"
MPDSCRCONFDIR_DEFAULT="${HOME}/.mpdscribble"
PROFILE_TRG="${PROFILE_EXTRA:-"${HOME}/.profile"}"

MPDMUSICDIR="${1:-"${MUSICHOME}"}"
MPDSCRCONFDIR="$(readlink -f "${3:-"${MPDSCRCONFDIR_DEFAULT}"}")"
MPDCONFDIR="$(readlink -f "${4:-"${MPDCONFDIR_DEFAULT}"}")"
MPDLOCALDIR="$(readlink -f "${5:-"${MPDLOCALDIR_DEFAULT}"}")"

MPDPLAYLISTDIR_DEFAULT="${MPDLOCALDIR}/playlists"
MPDPLAYLISTDIR="${2:-"${MPDPLAYLISTDIR_DEFAULT}"}"


pkill mpd &>/dev/null


mkdir -p "${MPDCONFDIR}" "${MPDPLAYLISTDIR}" "${MPDLOCALDIR}"  &&  touch "${MPDLOCALDIR}"/{database,sticker.sql}

read -s -p 'Password for clients to be able to control MPD > ' MPDPASSWD  &&  echo ""

"${DIR}/mpd-conf-generate.sh"          "${MPDPASSWD}" "${MPDMUSICDIR}" "${MPDPLAYLISTDIR}" "${MPDCONFDIR}" "${MPDLOCALDIR}"
"${DIR}/mpdcreds-generate.sh"          "${MPDPASSWD}" "${MPDCONFDIR}"
"${DIR}/mpdscribble-conf-generate.sh"  "${MPDPASSWD}" "${MPDSCRCONFDIR}"

sudo cp "/etc/mpd.conf"           "${MPDCONFDIR}/mpdconf.example"
sudo cp "/etc/mpdscribble.conf"   "${MPDSCRCONFDIR}/mpdscribbleconf.example"

mkdir -p "${MPDPLAYLISTDIR}"  &&  touch "${MPDPLAYLISTDIR}/[Radio Streams].m3u"



CREDSEXPORTFILE="${MPDCONFDIR}/creds/mpdcreds-export.sh"
echo -e "\nsource \"${CREDSEXPORTFILE/#$HOME/\$HOME}\""  >> "${PROFILE_TRG}"


mpd  &&  mpdscribble  &&  sleep 1s  &&  mpc --host="${MPDPASSWD}@localhost" update
