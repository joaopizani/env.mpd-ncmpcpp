#!/usr/bin/env bash
DIR="$(dirname "$(readlink -f "${BASH_SOURCE[0]}")" )"


sudo apt install mpd mpc mpdscribble cantata playerctl
sleep 1s


# We must enable neither system-wide not user-specific systemd units:
# That's because we want an instance of mpd run by the user, but even the pre-packaged user instance doesn't cut it:
# Something gets started before (too early) for some buggy reason, so we must use a timer (after bootsec)
sudo systemctl mask --now mpd.{service,socket} mpdscribble.service
systemctl --user mask --now mpd.socket
sudo rm -f "/etc/systemd/user/default.target.wants/mpd.desktop"
systemctl --user daemon-reload
systemctl --user stop mpd.service && pkill mpd
systemctl --user reset-failed
systemctl --user start mpd.service


"${DIR}/install.sh"


# So instead of systemd units, we just put a simple .desktop file in ~/.config/autostart
AUTOSTART_DIR="${XDG_CONFIG_HOME:-"${HOME}/.config"}/autostart"
MPDS_DESKTOP="startup/mpdscribble.desktop"
MPD_DESKTOP="startup/mpd.desktop"
mkdir -p "${AUTOSTART_DIR}"

ln -sfn "${DIR}/${MPDS_DESKTOP}" "${AUTOSTART_DIR}/${MPDS_DESKTOP}"
[[ "${DIR}/${MPDS_DESKTOP}" == "${HOME}"* ]]  &&  ln -sfnr "${DIR}/${MPDS_DESKTOP}" "${AUTOSTART_DIR}/${MPDS_DESKTOP}"

ln -sfn "${DIR}/${MPD_DESKTOP}" "${AUTOSTART_DIR}/${MPD_DESKTOP}"
[[ "${DIR}/${MPD_DESKTOP}" == "${HOME}"* ]]  &&  ln -sfnr "${DIR}/${MPD_DESKTOP}" "${AUTOSTART_DIR}/${MPD_DESKTOP}"

systemctl --user enable "${DIR}/startup/mpd.timer"
