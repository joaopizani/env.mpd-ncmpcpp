MPD/MPC/MPDScribble config
======================

Nice and self-contained configuration for a local-only or networked MPD server,
along with scrobbling to Last.fm (`mpdscribble`) and command-line client (`mpc`).

Some time ago I became tired of the ad-hoc nature and specificities of usual music players,
and decided to give [MPD (Music Player Daemon)](http://www.musicpd.org/) a try.
I had, however, a certain list of requirements that this new setup should have,
and fortunately all of them could be satisfied:

* Easy remote control from laptop / phone / etc
* Run as a service in the background
* Submit tracks to Last.fm (scrobble)

Being who I am, I meticulously fine-tuned the settings of both MPD and NCMPCPP in a way which was very convenient to use.
But also, the settings have a **gradient of customizability**.
The ideal I strive for is: the more personal the setting, the easier it should be to override the defaults.


Highlights of the configuration
-------------------------------

### Basic MPD configuration ###

 * There is already an MPD audio output defined for _PulseAudio_ on the local machine.
   So you should be able to listen to music without any extra config. Of course you **can** add other outputs.

 * Directories where things live have reasonable defaults, but can be chosen as parameters of the install script.
    + All music files reside by default `~/Music`.
    + Playlists by default live under `<music-dir>/playlists`.

### Suggested clients ###

 * **Linux/Windows/macOS:**  `cantata`
   + Has nice support of standard MPRIS controls.
     - So you can use keyboard media keys, bluetooth headset buttons, or software like `playerctl`
   + In all standard distro repositories, for Windows search in chocolatey repos.
 * **Android:**  `M.A.L.P`
   + Nice notification controls, lock screen support, works with controls from headphones etc.
   + Present in F-Droid repos.

### Listening for clients ###

 * When installing (more on installation below), there is a choice to run MPD _only locally_ or _open to the network_.
   + If run _locally only_, MPD listens to connections from _127.0.0.1_ and from **no other machines**.

   + If run _open to the net_, MPD listens to connections coming from anywhere.
     - In this way you can use (for example) a computer as a "music server" in the living room.
     - A password is asked during installation, to be put in MPD's config file, and it's needed for clients to connect.

   + **OBS:** If you want to restrict MPD to LAN-only, you must edit the line containing
     `bind_to_address "0.0.0.0"` in `mpd.conf` and insert the local IP of your server there.



How to install
--------------

  * For Debian-based systems, use `install-complete.sh`. This will install packages and do systemd service fine-tuning.
  * For any other distro flavours, you must install the following packages yourself:
    + To install: `mpd`, `mpdscribble`, `mpc`
    + This repository's config assumes that all system-wide ways of launching MPD and mpdscribble are disabled, so do that.
    + After installing packages and disable system-wise launching, run `install.sh`

